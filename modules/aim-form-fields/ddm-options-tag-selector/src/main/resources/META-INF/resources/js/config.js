;(function() {

    var base = MODULE_PATH + '/js/';

	AUI().applyConfig(
		{
			groups: {
				'ddm-options-tag-selector': {
					base: base,
					combine: Liferay.AUI.getCombine(),
					filter: Liferay.AUI.getFilterConfig(),
					modules: {
						'liferay-ddm-options-tag-selector': {
							condition: {
								trigger: 'liferay-ddm-form-renderer'
							},
							path: 'options_tag_selector.js',
                            requires: [
                                'aui-text-unicode',
                                'liferay-ddm-form-field-text',
                                'liferay-ddm-form-renderer-field',
                                'liferay-ddm-form-renderer-field',
                            ]
						}
					},
					root: base
				}
			}
		}
	);
})();