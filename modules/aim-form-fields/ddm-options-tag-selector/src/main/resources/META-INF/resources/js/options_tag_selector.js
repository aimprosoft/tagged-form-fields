AUI.add(
    'liferay-ddm-options-tag-selector',

    function (A) {

        var OptionsTagSelectorField = A.Component.create(
            {
                ATTRS: {
                    //todo: initialization
                },

                EXTENDS: Liferay.DDM.Field.Text,

                NAME: 'liferay-ddm-options-tag-selector',

                prototype: {

                    initializer: function () {

                        var instance = this;
                        console.log('KeyValueTaggedField initializer, instance: ', instance);

                        //todo: event handlers
                        /*instance._eventHandlers.push(
                            instance.after('keyChange', instance._afterKeyChange),
                            instance.after('valueChange', instance._afterValueChangeInput),
                            instance.bindContainerEvent('blur', instance._onBlurKeyInput, '.key-value-input'),
                            instance.bindContainerEvent('keyup', instance._onKeyUpKeyInput, '.key-value-input'),
                            instance.bindContainerEvent('valuechange', instance._onValueChangeKeyInput, '.key-value-input')
                        );*/
                    }
                }
            }
        );
        Liferay.namespace('DDM.Field').OptionsTagSelector = OptionsTagSelectorField;
    },
    '',
    {
        requires: [
            'aui-text-unicode',
            'event-valuechange',
            'liferay-ddm-form-field-text',
            'liferay-ddm-form-renderer-field'
        ]
    }
);