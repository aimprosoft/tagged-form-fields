;(function() {

    var base = MODULE_PATH + '/js/';

	AUI().applyConfig(
		{
			groups: {
				'ddm-options-aui-override': {
					base: base,
					combine: Liferay.AUI.getCombine(),
					filter: Liferay.AUI.getFilterConfig(),
					modules: {
						'liferay-ddm-form-field-options-override': { 					//module-override
							condition: {
                                name: 		'liferay-ddm-form-field-options-override', 	//module-override
                                trigger: 	'liferay-ddm-form-field-options', 			//original module
                                when: 		'instead'
							},
							path: 'options_field_override.js', 							//module.js
							requires: [
								'aui-sortable-list',
								'liferay-ddm-form-field-key-value',
								'liferay-ddm-form-renderer-field'
							]
						}
					},
					root: base
				}
			}
		}
	);
})();